Routes are dynamically allocated through the `index.js` file that is in the folder. 

This means that in order to require AND initialize all of the routes for our web app we only need one quick line: `require('./routes')(app);` this should also work with express, but more testing is required.