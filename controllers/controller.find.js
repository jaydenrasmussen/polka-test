'use strict';

const log = require('../utils/util.log');

module.exports = {
	root
}
function root(req, res) {
	if (req.params.find === undefined) {
		return res.end('Please provide a parameter to search');
	}
	return res.end(`Received: ${req.params.find}`) 
}