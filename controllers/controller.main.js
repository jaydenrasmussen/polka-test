'use strict';
const log = require('../utils/util.log');
module.exports = {
    root
};

function root(req, res) {
	res.statusCode = (200);
    return res.end('Welcome to the app!');
}
