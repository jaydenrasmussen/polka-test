'use strict';
const fs = require('fs');
module.exports = app => {
	let routes = readRoutes();
    Object.keys(routes).map(route => routes[route](app));
}
function readRoutes() {
	let routeArray = fs.readdirSync('./routes');
	let routeObject = {};
	for (let i = 0; i < routeArray.length; i++) {
		if (routeArray[i] !== 'index.js') {
			let route = routeArray[i];
			route = route.substr(6);
			route = route.slice(0, -3);
			routeObject[route] = require(`./${routeArray[i]}`);			
		}
	}
	return routeObject;
}