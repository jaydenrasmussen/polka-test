'use strict';

const log = require('../utils/util.log');
const controller = require('../controllers/controller.find');

module.exports = app => {
	app.get('/find/:find?', (req, res) => {
		controller.root(req, res);
	});
}