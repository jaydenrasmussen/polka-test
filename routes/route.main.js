'use strict';

const controller = require('../controllers/controller.main');
const find = require('./route.find');

module.exports = app => {
    app.get('/', (req, res) => {
    	controller.root(req, res)
    });
};
