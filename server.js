'use strict';

const app = require('polka')();
const log = require('./utils/util.log');
const routes = require('./routes')(app);

module.exports = (() => {
    let port = process.env.PORT || 8080;
    app.listen(port).then(log.success(`App is running on localhost:${port}/`));
})();
