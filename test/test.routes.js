'use strict';

const test = require('ava');
const execa = require('execa');
const request = require('superagent');

test.before(async () => {
	execa.shell('node ./server');
});

test('/ should return a 200', async t => {
	let result = await request.get('localhost:8080/').end();
	t.is(result, 'Welcome to the app!');
});