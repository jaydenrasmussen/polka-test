'use strict';
const m = require('moment');
const c = require('chalk');
module.exports = {
    error,
    info,
    success,
    warn
};
function valid(string) {
    return string ? string : '';
}
function date() {
    return m().format('L LTS');
}
function error(string) {
    return console.log(`${c.red('[ERROR]')}    ${date()}  ${valid(string)}`);
}
function info(string) {
    return console.log(`${c.blue('[INFO]')}     ${date()}  ${valid(string)}`);
}
function success(string) {
    return console.log(`${c.green('[SUCCESS]')}  ${date()}  ${valid(string)}`);
}
function warn(string) {
    return console.log(`${c.yellow('[WARN]')}     ${date()}  ${valid(string)}`);
}
